<?php

class About extends Controller
{
    public function index($nama = 'Atoen', $profesi = 'Writer')
    {
        $data['nama'] = $nama;
        $data['profesi'] = $profesi;
        // echo "Perkenalkan saya adalah $nama dan kerjaan saya $profesi";
        $this->view('About/index', $data);
    }
    public function page()
    {
        echo 'About/Page';
    }
}
