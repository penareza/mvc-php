<?php

class Home extends Controller
{

    public function index()
    {
        $data['nama_halaman'] = 'Home';
        $this->view('layout/header', $data);
        $this->view('Home/index');
        $this->view('layout/footer');
    }

    public function berita()
    {
        $data['nama_halaman'] = 'Berita';
        $this->view('layout/header', $data);
        $this->view('Home/berita');
        $this->view('layout/footer');
    }
}
