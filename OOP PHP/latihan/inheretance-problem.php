<?php
class kursus
{
    // membuat property
    public $jurusan, $harga, $instruktur, $jp, $ket, $tipe;

    public function __construct(
        $jurusan = "jurusan",
        $harga = "harga",
        $instruktur = "instruktur",
        $jp = 0,
        $ket = 'Paket',
        $tipe = 'tipe'
    ) {
        $this->jurusan = $jurusan;
        $this->harga = $harga;
        $this->instruktur = $instruktur;
        $this->jp = $jp;
        $this->ket = $ket;
        $this->tipe = $tipe;
    }
    //method
    public function getData()
    {
        return "$this->jurusan, $this->instruktur";
    }

    public function Infokursuslengkap()
    {
        // Pemrograman : web | muh. adhan | Rp.(450000) - 50 Jp
        $str = "{$this->tipe} | {$this->getData()} Rp. ({$this->harga})";

        if ($this->tipe == "pemrograman") {
            $str .= "- {$this->jp} Jp.";
        } elseif ($this->tipe == "Dasar") {
            $str .= "- {$this->ket} ";
        }
        return $str;
    }
}

class CetakInfoKursus
{
    public function cetak(kursus $kursus)
    {
        $str = "{$kursus->getData()} | {$kursus->harga}";
        return $str;
    }
}


$kursus1 = new kursus("web", 450000, "Muh. Adhan", 50, "paket", "pemrograman");
$kursus2 = new kursus("Word", 500000, "Ardiman", 0, "Paket", "Dasar");

echo $kursus1->Infokursuslengkap();
echo "<br>";
echo $kursus2->Infokursuslengkap();
