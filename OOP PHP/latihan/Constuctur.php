<?php
class kursus
{
    // membuat property
    public $jurusan, $harga, $instruktur;

    public function __construct($jurusan = "jurusan", $harga = "harga", $instruktur = "instruktur")
    {
        $this->jurusan = $jurusan;
        $this->harga = $harga;
        $this->instruktur = $instruktur;
    }
    //method
    public function getData()
    {
        return "$this->jurusan, $this->instruktur";
    }
}


$kursus1 = new kursus("Pemrograman", 450000, "Muh. Adhan");
$kursus2 = new kursus("Aplikasi Perkntoran", 500000, "Ardiman");
$kursus3 = new kursus("desain");


echo "Jurusan" . $kursus1->getData();
echo '<br>';
echo "Jurusan " . $kursus2->getData();
echo '<br>';
var_dump($kursus3);
