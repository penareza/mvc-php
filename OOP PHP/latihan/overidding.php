<?php
class kursus
{
    // membuat property
    public $jurusan, $harga, $instruktur, $jp, $ket;

    public function __construct(
        $jurusan = "jurusan",
        $harga = "harga",
        $instruktur = "instruktur",
        $jp = 0,
        $ket = 'Paket'

    ) {
        $this->jurusan = $jurusan;
        $this->harga = $harga;
        $this->instruktur = $instruktur;
        $this->jp = $jp;
        $this->ket = $ket;
    }
    //method
    public function getData()
    {
        return "$this->jurusan, $this->instruktur";
    }

    public function Infokursuslengkap()
    {
        // Pemrograman : web | muh. adhan | Rp.(450000) - 50 Jp
        $str = "{$this->getData()} Rp. ({$this->harga})";

        return $str;
    }
}

class kursuspemrograman extends kursus
{
    public function Infokursuslengkap()
    {
        $str = " Pemrograman: " . parent::InfokursusLengkap() . " - {$this->jp} Jam.";
        return $str;
    }
}

class kursusgrafis extends kursus
{
    public function Infokursuslengkap()
    {
        $str = " Graphic: " . parent::InfokursusLengkap() . " - {$this->ket} ";
        return $str;
    }
}

class CetakInfoKursus
{
    public function cetak(kursus $kursus)
    {
        $str = "{$kursus->getData()} | {$kursus->harga}";
        return $str;
    }
}


$kursus1 = new kursuspemrograman("web", 450000, "Muh. Adhan", 50, "paket");
$kursus2 = new kursusgrafis("Word", 500000, "Ardiman", 0, "Paket");

echo $kursus1->Infokursuslengkap();
echo "<br>";
echo $kursus2->Infokursuslengkap();
