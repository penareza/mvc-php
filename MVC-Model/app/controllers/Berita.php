<?php

class Berita extends Controller
{
    function index()
    {
        $data['nama_halaman'] = 'Data Berita';
        $data['berita'] = $this->model('Berita_model')->getAllnews();
        $this->view('layout/header', $data);
        $this->view('Berita/index', $data);
        $this->view('layout/footer');
    }
}
