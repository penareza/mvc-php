<?php

class Home extends Controller
{



    public function index()
    {

        $data['nama_halaman'] = 'Home';
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('layout/header', $data);
        $this->view('Home/index', $data);
        $this->view('layout/footer');
    }

    public function berita()
    {
        $data['nama_halaman'] = 'Berita';
        $this->view('layout/header', $data);
        $this->view('Home/berita');
        $this->view('layout/footer');
    }
}
